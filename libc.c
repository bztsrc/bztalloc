/*
 * libc.c
 *
 * Copyright (C) 2018 bzt (bztsrc@gitlab)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief Example bztalloc integration with an arbitrary libc
 */

/* this should be provided by the linker script, and must be page aligned */
extern char _dynbss_start;

/* these must be implemented in your libc */
extern void memzero (void *dest, size_t n);
extern void *memcpy (void *dest, void *src, size_t n);
/* POSIX compliant interface. Should call vmm_alloc() in your kernel through a system call. */
extern void *mmap (void *addr, size_t len, int prot, int flags, int fd, off_t offs);
extern int munmap (void *addr, size_t len);

/* the bss must have at least one empty page mapped in. Could be provided by the run-time linker
 * or the page fault handler too. Your choice. */
void libc_init_malloc()
{
    mmap((void*)_dynbss_start, PAGESIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED | MAP_ANONYMOUS, -1, 0);
    memzero((void*)_dynbss_start, PAGESIZE);
}

/* defines required by bztalloc. Single threaded, no shared memory. See README.md for more options. */
#define PAGESIZE            4096
#define seterr(x)           errno=x
#define lockacquire(b,p)
#define lockrelease(b,p)

/* memory allocation in your libc, provided by bztalloc. You can use static functions instead if you'd like */
#define malloc(s)          bzt_alloc((void*)_dynbss_start,8,NULL,s,MAP_PRIVATE)
#define calloc(n,s)        bzt_alloc((void*)_dynbss_start,8,NULL,n*s,MAP_PRIVATE)
#define realloc(p,s)       bzt_alloc((void*)_dynbss_start,8,p,s,MAP_PRIVATE)
#define aligned_alloc(a,s) bzt_alloc((void*)_dynbss_start,a,NULL,s,MAP_PRIVATE)
#define free(p)            bzt_free ((void*)_dynbss_start,p)
